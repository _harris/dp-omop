
-- Example DDL for storing results of linking geocoding results to census geographies

-- DROP TABLE dp.omop_census_link;

CREATE TABLE dp.omop_census_link (
	location_id int4 NULL,
	longitude float8 NULL,
	latitude float8 NULL,
	block varchar(100) NULL,
	blockgroup varchar(100) NULL,
	tract varchar(100) NULL,
	county varchar(100) NULL,
	state varchar(100) NULL
);
