-------
------- 
-------
------- dp_census_link will link OMOP's location table to census geographies
-------	Note: these are saved in an ancillary table: omop_census_link
-------
------- Requirements: PostGIS
-------
------- 

create or replace procedure dp.dp_census_link language sql as 
$$
insert into dp.omop_census_link (location_id, longitude, latitude) select distinct location_id, longitude, latitude from dp.omop_location ol;
update dp.omop_census_link pao
set (block, blockgroup, tract, county, state)
	= (a.block, a.blockgroup, a.tract, a.county, a.state)
from (select oa.location_id, oa.longitude, oa.latitude, 
		t.tabblock_id as block,
		substring(t.tabblock_id from 1 for 12) as blockgroup,
		substring(t.tabblock_id from 1 for 11) as tract,
		substring(t.tabblock_id from 1 for 5) as county,
		substring(t.tabblock_id from 1 for 2) as state
	from dp.omop_location oa
		left join tabblock t on ST_Contains(the_geom, ST_SetSRID(ST_Point(oa.longitude, oa.latitude), 4269))
) as a
where pao.location_id = a.location_id and pao.block is null;
$$

-------
------- 
------- Example call: 
------- 
--call dp.dp_census_link();

