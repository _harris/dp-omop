-------
------- 
-------
------- omop_geocode will call PostGIS functions to geocode OMOP's location table
-------		
------- Requirements: Google's differential privacy library
-------
------- 

create or replace procedure dp.omop_geocode() language plpgsql as 
$$
DECLARE i int;
BEGIN
i = 0;
WHILE i < 50
LOOP
        UPDATE gis2019_all.dp.omop_location ao
          SET  (longitude, latitude)
                = (  ST_X(g.geomout), ST_Y(g.geomout) )
        FROM (
                SELECT location_id, o.address_1  || ', ' || o.city || o.state || ', ' || o.zip as addr_full_line
                from gis2019_all.dp.omop_location o
                WHERE latitude IS NULL ORDER BY location_id LIMIT 1000
        ) As a
                LEFT JOIN LATERAL geocode(a.addr_full_line,1) As g ON true
        WHERE a.location_id = ao.location_id;
        COMMIT;
        i = i + 1;
END LOOP;
end;
$$;

-------
------- 
------- Example call:  no parameters needed, assumes table is dp_omop_location
------- 
-- call dp.omop_geocode();