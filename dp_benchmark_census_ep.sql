
-------
------- 
-------
------- dp_benchmark_census_ep will run several iterations of ANON_COUNT
-------		and record the results into a basic table.
------- Requirements: Google's differential privacy library
-------
------- 
create or replace procedure dp.dp_benchmark_census_ep(i float, m float, step float) language plpgsql as 
$$
--DECLARE i float;
begin
truncate dp.dp_benchmark_ep;
--i = 0.1;
--WHILE i <= 1.5
WHILE i <= m and i < 2
loop
	insert into dp.dp_benchmark_ep
	with x as (
		select tract, i as ep, count(*) as raw_total,
								greatest(0, ANON_COUNT(location_id,i)) as anon_total
		from gis2019_all.dp.omop_census_link
		where state = '12'
		group by tract 
	)
	select *, (anon_total - raw_total) as diff
	from x;
        i = i + step;
END LOOP;
end;
$$;

-------
------- 
------- Example call:  from 0.1 to 2, step by 0.1
------- 
-- call dp.dp_benchmark_census_ep(0.1, 2, 0.1);
