# README #

This project leverages an open-source library for differential privacy tasks and PostGIS for geospatial tasks.

We provide database procedures that help connect differential privacy to PostGIS to be used with standardized healthcare data.

### OMOP ###
We depend upon the location table associated with persons in the Observational Medical Outcomes Partnership (OMOP) common data model.

### REQUIREMENTS ###
* PostgreSQL, PostGIS, Google's differential privacy library
* Some functions write to tables (example DDLs are given)