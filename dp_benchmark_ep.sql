-------
------- 
-------
------- dp_benchmark_ep will run several iterations of ANON_COUNT
-------		and record the results into a basic table.
------- Requirements: Google's differential privacy library
-------
------- 
create or replace procedure dp.dp_benchmark_ep(i float, m float, step float) language plpgsql as 
$$
begin
truncate dp.dp_benchmark_ep;
WHILE i <= m and i < 2
loop
	insert into dp.dp_benchmark_ep
	with x as (
		select state, i as ep, count(*) as raw_total,
								greatest(0, ANON_COUNT(location_id,i)) as anon_total
		from dp.omop_location ols
		group by state 
	)
	select *, (anon_total - raw_total) as diff
	from x;
        i = i + step;
END LOOP;
end;
$$;

-------
------- 
------- Example call:  from 0.1 to 2, step by 0.1
------- 
-- call dp.dp_benchmark_ep(0.1, 2, 0.1);
