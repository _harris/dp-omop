
-- Example table for holding benchmark results

-- DROP TABLE dp.dp_benchmark_ep;

CREATE TABLE dp.dp_benchmark_ep (
	state text NULL,
	ep numeric NULL,
	raw_total int8 NULL,
	anon_total int8 NULL,
	diff int8 NULL
);
